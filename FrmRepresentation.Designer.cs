﻿namespace Grafy_Bednarczyk
{
    partial class FrmRepresentation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.comboBoxRepresentation = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(324, 191);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // comboBoxRepresentation
            // 
            this.comboBoxRepresentation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxRepresentation.FormattingEnabled = true;
            this.comboBoxRepresentation.Items.AddRange(new object[] {
            "Reprezentacja macierzowa",
            "Reprezentacja listowa"});
            this.comboBoxRepresentation.Location = new System.Drawing.Point(277, 134);
            this.comboBoxRepresentation.Name = "comboBoxRepresentation";
            this.comboBoxRepresentation.Size = new System.Drawing.Size(159, 24);
            this.comboBoxRepresentation.TabIndex = 1;
            // 
            // FrmRepresentation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.comboBoxRepresentation);
            this.Controls.Add(this.buttonOK);
            this.Name = "FrmRepresentation";
            this.Text = "FrmRepresentation";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.ComboBox comboBoxRepresentation;
    }
}