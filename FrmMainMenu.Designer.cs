﻿namespace Grafy_Bednarczyk
{
    partial class FrmMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRead = new System.Windows.Forms.Button();
            this.buttonDisplayMatrix = new System.Windows.Forms.Button();
            this.buttonDisplayList = new System.Windows.Forms.Button();
            this.labelDisplay = new System.Windows.Forms.Label();
            this.buttonDijkstra = new System.Windows.Forms.Button();
            this.buttonBellmanFord = new System.Windows.Forms.Button();
            this.buttonPrim = new System.Windows.Forms.Button();
            this.buttonKruskal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonRead
            // 
            this.buttonRead.Location = new System.Drawing.Point(63, 62);
            this.buttonRead.Name = "buttonRead";
            this.buttonRead.Size = new System.Drawing.Size(131, 86);
            this.buttonRead.TabIndex = 0;
            this.buttonRead.Text = "Wczytaj graf z pliku";
            this.buttonRead.UseVisualStyleBackColor = true;
            this.buttonRead.Click += new System.EventHandler(this.buttonRead_Click);
            // 
            // buttonDisplayMatrix
            // 
            this.buttonDisplayMatrix.Location = new System.Drawing.Point(41, 169);
            this.buttonDisplayMatrix.Name = "buttonDisplayMatrix";
            this.buttonDisplayMatrix.Size = new System.Drawing.Size(120, 71);
            this.buttonDisplayMatrix.TabIndex = 1;
            this.buttonDisplayMatrix.Text = "Wyświetl graf macierzowo";
            this.buttonDisplayMatrix.UseVisualStyleBackColor = true;
            this.buttonDisplayMatrix.Click += new System.EventHandler(this.buttonDisplayMatrix_Click);
            // 
            // buttonDisplayList
            // 
            this.buttonDisplayList.Location = new System.Drawing.Point(41, 272);
            this.buttonDisplayList.Name = "buttonDisplayList";
            this.buttonDisplayList.Size = new System.Drawing.Size(120, 66);
            this.buttonDisplayList.TabIndex = 2;
            this.buttonDisplayList.Text = "Wyświetl graf listowo";
            this.buttonDisplayList.UseVisualStyleBackColor = true;
            this.buttonDisplayList.Click += new System.EventHandler(this.buttonDisplayList_Click);
            // 
            // labelDisplay
            // 
            this.labelDisplay.AutoSize = true;
            this.labelDisplay.Location = new System.Drawing.Point(318, 297);
            this.labelDisplay.Name = "labelDisplay";
            this.labelDisplay.Size = new System.Drawing.Size(0, 17);
            this.labelDisplay.TabIndex = 3;
            // 
            // buttonDijkstra
            // 
            this.buttonDijkstra.Location = new System.Drawing.Point(412, 62);
            this.buttonDijkstra.Name = "buttonDijkstra";
            this.buttonDijkstra.Size = new System.Drawing.Size(119, 35);
            this.buttonDijkstra.TabIndex = 4;
            this.buttonDijkstra.Text = "Dijkstra";
            this.buttonDijkstra.UseVisualStyleBackColor = true;
            this.buttonDijkstra.Click += new System.EventHandler(this.buttonDijkstra_Click);
            // 
            // buttonBellmanFord
            // 
            this.buttonBellmanFord.Location = new System.Drawing.Point(412, 116);
            this.buttonBellmanFord.Name = "buttonBellmanFord";
            this.buttonBellmanFord.Size = new System.Drawing.Size(119, 56);
            this.buttonBellmanFord.TabIndex = 5;
            this.buttonBellmanFord.Text = "Bellman-Ford";
            this.buttonBellmanFord.UseVisualStyleBackColor = true;
            this.buttonBellmanFord.Click += new System.EventHandler(this.buttonBellmanFord_Click);
            // 
            // buttonPrim
            // 
            this.buttonPrim.Location = new System.Drawing.Point(412, 188);
            this.buttonPrim.Name = "buttonPrim";
            this.buttonPrim.Size = new System.Drawing.Size(119, 47);
            this.buttonPrim.TabIndex = 6;
            this.buttonPrim.Text = "Prim";
            this.buttonPrim.UseVisualStyleBackColor = true;
            this.buttonPrim.Click += new System.EventHandler(this.buttonPrim_Click);
            // 
            // buttonKruskal
            // 
            this.buttonKruskal.Location = new System.Drawing.Point(412, 258);
            this.buttonKruskal.Name = "buttonKruskal";
            this.buttonKruskal.Size = new System.Drawing.Size(119, 46);
            this.buttonKruskal.TabIndex = 7;
            this.buttonKruskal.Text = "Kruskal";
            this.buttonKruskal.UseVisualStyleBackColor = true;
            this.buttonKruskal.Click += new System.EventHandler(this.buttonKruskal_Click);
            // 
            // FrmMainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 490);
            this.Controls.Add(this.buttonKruskal);
            this.Controls.Add(this.buttonPrim);
            this.Controls.Add(this.buttonBellmanFord);
            this.Controls.Add(this.buttonDijkstra);
            this.Controls.Add(this.labelDisplay);
            this.Controls.Add(this.buttonDisplayList);
            this.Controls.Add(this.buttonDisplayMatrix);
            this.Controls.Add(this.buttonRead);
            this.Name = "FrmMainMenu";
            this.Text = "Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRead;
        private System.Windows.Forms.Button buttonDisplayMatrix;
        private System.Windows.Forms.Button buttonDisplayList;
        private System.Windows.Forms.Label labelDisplay;
        private System.Windows.Forms.Button buttonDijkstra;
        private System.Windows.Forms.Button buttonBellmanFord;
        private System.Windows.Forms.Button buttonPrim;
        private System.Windows.Forms.Button buttonKruskal;
    }
}

