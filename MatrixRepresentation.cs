﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy_Bednarczyk
{
    public class MatrixRepresentation : Display
    {
        public int Node { get; set; }
        public int Edge { get; set; }
        public int Start { get; set; }
        public int End { get; set; }
        public int[,] Matrix { get; set; }

        public MatrixRepresentation(int n, int e, int s, int end)
        {
            Node = n;
            Edge = e;
            Start = s;
            End = end;

            Matrix = new int[Node, Node];
            //zerowanie macierzy
            for (int i = 0; i < Node; i++)
            {
                for (int j = 0; j < Node; j++)
                {
                    Matrix[i, j] = 0;
                }
            }
        }

        //tworzenie reprezzaentacji macierzowej
        public void AddMatrix(int start, int stop, int value)
        {
            Matrix[start, stop] = value;
        }

        //wyswietlanie grafu
        public string DisplayMatrix()
        {
            string graph="";
            graph += "         ";
            for (int i = 0; i < Node; i++)
            {
                graph += i + "   ";
            }
            graph += "\n\n";
            for (int i = 0; i < Node; i++)
            {
                graph += i + "       ";
                for(int j=0; j< Node; j++)
                {
                    graph += Matrix[i, j] + "   ";
                }
                graph += "\n";
            }
            return graph;
        }

        // wyznaczanie minimalnego drzewa rozpinającego (MST) – algorytm Kruskala
        public string CountKruskal()
        {
            List<int[]> tmpList = new List<int[]>();
            List<int[]> result = new List<int[]>();
            List<List<int>> listOfTrees = new List<List<int>>();
            bool[] nodesInMST = new bool[Node];
            bool check = true;
            List<int> tmp = new List<int>();
            for(int i=0; i< Node; i++)
            {
                check = true;
                nodesInMST[i] = false;
                for(int j=0; j<Node; j++)
                {
                    if(Matrix[i,j] != 0)  tmpList.Add(new int[3] {i, j, Matrix[i, j]});

                }
            }
            var sortedList = tmpList.OrderBy(r => r[2]);

            for(int i =0; i< Edge; i++)
            {
                int[] tmpValue = new int[3];
                tmpValue = sortedList.ElementAt(i);
                check = true;

                if (((tmp.Contains(tmpValue[0]) || tmp.Contains(tmpValue[1])) && !(tmp.Contains(tmpValue[0]) && tmp.Contains(tmpValue[1]))))
                {
                    bool done = false;
                    for (int j = 0; j < listOfTrees.Count; j++)
                    {
                        for (int k = 0; k < listOfTrees[j].Count; k++)
                        {
                            if (listOfTrees[j][k] == tmpValue[0])
                            {
                                listOfTrees[j].Add(tmpValue[1]);
                                tmp.Add(tmpValue[1]);
                                nodesInMST[tmpValue[1]] = true;
                                result.Add(tmpValue);
                                done = true;
                                break;
                            }
                            if (listOfTrees[j][k] == tmpValue[1])
                            {
                                listOfTrees[j].Add(tmpValue[0]);
                                tmp.Add(tmpValue[0]);
                                nodesInMST[tmpValue[0]] = true;
                                result.Add(tmpValue);
                                done = true;
                                break;
                            }
                        }
                        if (done) break;
                    }
                }
                else if (!(tmp.Contains(tmpValue[0]) || tmp.Contains(tmpValue[1])))
                {
                    listOfTrees.Add(new List<int>());
                    listOfTrees[listOfTrees.Count-1].Add(tmpValue[0]);
                    listOfTrees[listOfTrees.Count-1].Add(tmpValue[1]);
                    tmp.Add(tmpValue[0]);
                    tmp.Add(tmpValue[1]);
                    nodesInMST[tmpValue[0]] = true;
                    nodesInMST[tmpValue[1]] = true;
                    result.Add(tmpValue);
                }
                else if (tmp.Contains(tmpValue[0]) && tmp.Contains(tmpValue[1]))
                {
                    int first =0, second = 0;
                    bool contain = true;
                    for (int j = 0; j < listOfTrees.Count; j++)
                    {
                        if(listOfTrees[j].Contains(tmpValue[0]) && listOfTrees[j].Contains(tmpValue[1]))
                        {
                            contain = false;
                            break;
                        }
                        if (listOfTrees[j].Contains(tmpValue[0])) first = j;
                        if (listOfTrees[j].Contains(tmpValue[1])) second = j;
                    }
                    if (contain)
                    {
                        if(first>second)
                        {
                            int tmpVal = first;
                            first = second;
                            second = tmpVal;
                        }
                        if (listOfTrees[first].Contains(tmpValue[0]))
                        {
                            nodesInMST[tmpValue[1]] = true;
                        }
                        else nodesInMST[tmpValue[0]] = true;
                        for (int j = 0; j < listOfTrees[second].Count; j++)
                        {
                            if (!listOfTrees[first].Contains(listOfTrees[second][j])) listOfTrees[0].Add(listOfTrees[second][j]);
                        }
                        listOfTrees.RemoveAt(second);
                        result.Add(tmpValue);
                    }
                }
                //jesli zawiera jeden z wierzcholkow to dodaj
                //jesli zawiera 2 wierzcholki to sprawdz czy te wierzcholki nie maja wspolnych
                foreach (var item in nodesInMST)
                {
                    if (item == false)
                    {
                        check = false;
                        break;
                    }
                }
                if (check) break;
            }

            string returnResult = "Lista krawędzi drzewa drzewa rozpinającego:\n";
            int sum = 0;
            for (int i = 0; i < result.Count; i++)
            {
                returnResult += result[i][0] + " , " + result[i][1] + "\n";
                sum += result[i][2];
            }
            returnResult += "Suma wag krawędzi: " + sum;

            return returnResult;
        }

        // wyznaczanie minimalnego drzewa rozpinającego (MST) – algorytm Prima
        public string CountPrim()
        {
            const int MAX = 2000000000;
            int[] key = new int[Node];
            int[] p = new int[Node];
            bool[] visited = new bool[Node];
            int[,] result = new int[2, Node];
            int minimum, tmpValue;

            for (int i = 0; i < Node; i++)
            {
                key[i] = MAX;
                p[i] = -1;
                visited[i] = false;
            }
            key[0] = 0;
            int tmpNode = 0;

            for(int i=0; i < Node - 1; i++)
            {
                minimum = 2000000000;
                tmpValue = 2000000000;
                visited[tmpNode] = true;
                for(int j = 0; j < Node; j++)
                {
                    if(Matrix[tmpNode,j] != 0)
                    {
                        if(!visited[j])
                        {
                            if(Matrix[tmpNode,j] < key[j])
                            {
                                key[j] = Matrix[tmpNode, j];
                                p[j] = tmpNode;
                            }
                        }
                    }
                }
                for(int j=0; j< Node; j++)
                {
                    if (key[j] < tmpValue && !visited[j])
                    {
                        tmpValue = key[j];
                        minimum = j;
                    }
                }
                tmpNode = minimum;

            }

            for (int i = 0; i < Node; i++)
            {
                result[0, i] = key[i];
                result[1, i] = p[i];
            }

            return DisplayMST(result);
        }

        //wyznaczanie najkrótszej ścieżki w grafie - algorytm Dijkstry
        public string CountDijkstra()
        {
            for (int i = 0; i < Node; i++)
            {
                for (int j = 1; j < Node; j++)
                {
                    if (Matrix[i,j] < 0) return "W grafie wystepują krawędzie z ujemnymi wagami\nAlgorytm Dijkstry nie zadziała";
                }
            }
            const int MAX = 2000000000;
            int[] nodes = new int[Node];
            int[] d = new int[Node];
            int[] p = new int[Node];
            int[,] result = new int[2, Node];

            for (int i=0; i<Node; i++)
            {
                nodes[i] = i;
                d[i] = MAX;
                p[i] = -1;
            }
            d[Start] = 0;
            int tmpNode = Start;
            for(int i=0; i<Node; i++)
            {
                for(int j=0; j<Node; j++)
                {
                    if (nodes[j] != -1 && d[j] < d[tmpNode]) tmpNode = nodes[j];
                }

                for (int j = 0; j < Node; j++)
                {
                    if (Matrix[tmpNode, j] != 0)
                    {
                        int tmpValue = d[tmpNode] + Matrix[tmpNode, j];
                        if (tmpValue < d[j])
                        {
                            d[j] = tmpValue;
                            p[j] = tmpNode;
                        }
                    }
                }
                nodes[tmpNode] = -1;
                for (int j = 0; j < Node; j++)
                {
                    if (nodes[j] != -1)
                    {
                        tmpNode = nodes[j];
                        break;
                    }
                }
            }

            for (int i = 0; i < Node; i++)
            {
                result[0, i] = d[i];
                result[1, i] = p[i];
            }

            return DisplayShortestTrack(result, Start);
        }
        //wyznaczanie najkrótszej ścieżki w grafie - algorytm Bellmanna-Forda
        public string CountBellmann_Ford()
        {
            const int MAX = 2000000000;
            int[] d = new int[Node];
            int[] p = new int[Node];
            int[,] result = new int[2, Node];
            bool changed = false;

            for (int i = 0; i < Node; i++)
            {
                d[i] = MAX;
                p[i] = -1;
            }
            d[Start] = 0;

            for(int k=0; k< Node-1; k++)
            {
                for (int i = 0; i < Node; i++)
                {
                    for (int j = 0; j < Node; j++)
                    {
                        if (Matrix[i, j] != 0)
                        {
                            int tmpValue = d[i] + Matrix[i, j];
                            if (tmpValue < d[j])
                            {
                                d[j] = tmpValue;
                                p[j] = i;
                                changed = true;
                            }
                        }
                    }
                }
                if (changed == false) break;
                changed = false;
            }
            //ujemny cykl
            for (int i = 0; i < Node; i++)
            {
                for (int j = 0; j < Node; j++)
                {
                    if (Matrix[i, j] != 0)
                    {
                        int tmpValue = d[i] + Matrix[i, j];
                        if (tmpValue < d[j]) return "W grafie występuje cykl ujemny";
                    }
                }
            }

            for (int i = 0; i < Node; i++)
            {
                result[0, i] = d[i];
                result[1, i] = p[i];
            }

            return DisplayShortestTrack(result, Start);
        }
    }
}
