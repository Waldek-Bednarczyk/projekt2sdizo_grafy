﻿namespace Grafy_Bednarczyk
{
    partial class FrmDirectedOrUndirected
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxDecision = new System.Windows.Forms.ComboBox();
            this.buttonConfirm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBoxDecision
            // 
            this.comboBoxDecision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDecision.FormattingEnabled = true;
            this.comboBoxDecision.Items.AddRange(new object[] {
            "Skierowany",
            "Nieskierowany"});
            this.comboBoxDecision.Location = new System.Drawing.Point(187, 41);
            this.comboBoxDecision.Name = "comboBoxDecision";
            this.comboBoxDecision.Size = new System.Drawing.Size(121, 24);
            this.comboBoxDecision.TabIndex = 0;
            // 
            // buttonConfirm
            // 
            this.buttonConfirm.Location = new System.Drawing.Point(203, 127);
            this.buttonConfirm.Name = "buttonConfirm";
            this.buttonConfirm.Size = new System.Drawing.Size(75, 23);
            this.buttonConfirm.TabIndex = 1;
            this.buttonConfirm.Text = "OK";
            this.buttonConfirm.UseVisualStyleBackColor = true;
            this.buttonConfirm.Click += new System.EventHandler(this.buttonConfirm_Click);
            // 
            // FrmDirectedOrUndirected
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(521, 225);
            this.Controls.Add(this.buttonConfirm);
            this.Controls.Add(this.comboBoxDecision);
            this.Name = "FrmDirectedOrUndirected";
            this.Text = "DirectedOrUndirected";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxDecision;
        private System.Windows.Forms.Button buttonConfirm;
    }
}