﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy_Bednarczyk
{
    public class ListRepresentation : Display
    {
        public int Node { get; set; }
        public int Edge { get; set; }
        public int Start { get; set; }
        public int End { get; set; }

        List<List<int>> list = new List<List<int>>();

        public ListRepresentation(int n, int e, int s, int end)
        {
            Node = n;
            Edge = e;
            Start = s;
            End = end;
            for (int i = 0; i < Node; i++) list.Add(new List<int>());
        }

        public void AddList(int start, int stop, int value)
        {
            list[start].Add(stop);
            list[start].Add(value);
        }

        //wyswietlanie grafu
        public string DisplayList()
        {
            string graph = "";
            for(int i=0; i< Node; i++)
            {
                graph += "List[" + i + "] =  ";
                for(int j=0; j<list[i].Count; j+=2)
                {
                    graph += list[i][j] + ":" + list[i][j+1] + "  ";
                }
                graph += "\n";
            }
            return graph;
        }

        //wyznaczanie minimalnego drzewa rozpinającego(MST) – algorytm Kruskala
        public string CountKruskal()
        {
            List<int[]> tmpList = new List<int[]>();
            List<int[]> result = new List<int[]>();
            List<List<int>> listOfTrees = new List<List<int>>();
            bool[] nodesInMST = new bool[Node];
            bool check = true;
            List<int> tmp = new List<int>();
            for (int i = 0; i < Node; i++)
            {
                check = true;
                nodesInMST[i] = false;
                for (int j = 0; j < list[i].Count; j+=2)
                {
                   tmpList.Add(new int[3] { i, list[i][j], list[i][j+1] });
                }
            }
            var sortedList = tmpList.OrderBy(r => r[2]);

            for (int i = 0; i < Edge; i++)
            {
                int[] tmpValue = new int[3];
                tmpValue = sortedList.ElementAt(i);
                check = true;

                if (((tmp.Contains(tmpValue[0]) || tmp.Contains(tmpValue[1])) && !(tmp.Contains(tmpValue[0]) && tmp.Contains(tmpValue[1]))))
                {
                    bool done = false;
                    for (int j = 0; j < listOfTrees.Count; j++)
                    {
                        for (int k = 0; k < listOfTrees[j].Count; k++)
                        {
                            if (listOfTrees[j][k] == tmpValue[0])
                            {
                                listOfTrees[j].Add(tmpValue[1]);
                                tmp.Add(tmpValue[1]);
                                nodesInMST[tmpValue[1]] = true;
                                result.Add(tmpValue);
                                done = true;
                                break;
                            }
                            if (listOfTrees[j][k] == tmpValue[1])
                            {
                                listOfTrees[j].Add(tmpValue[0]);
                                tmp.Add(tmpValue[0]);
                                nodesInMST[tmpValue[0]] = true;
                                result.Add(tmpValue);
                                done = true;
                                break;
                            }
                        }
                        if (done) break;
                    }
                }
                else if (!(tmp.Contains(tmpValue[0]) || tmp.Contains(tmpValue[1])))
                {
                    listOfTrees.Add(new List<int>());
                    listOfTrees[listOfTrees.Count - 1].Add(tmpValue[0]);
                    listOfTrees[listOfTrees.Count - 1].Add(tmpValue[1]);
                    tmp.Add(tmpValue[0]);
                    tmp.Add(tmpValue[1]);
                    nodesInMST[tmpValue[0]] = true;
                    nodesInMST[tmpValue[1]] = true;
                    result.Add(tmpValue);
                }
                else if (tmp.Contains(tmpValue[0]) && tmp.Contains(tmpValue[1]))
                {
                    int first = 0, second = 0;
                    bool contain = true;
                    for (int j = 0; j < listOfTrees.Count; j++)
                    {
                        if (listOfTrees[j].Contains(tmpValue[0]) && listOfTrees[j].Contains(tmpValue[1]))
                        {
                            contain = false;
                            break;
                        }
                        if (listOfTrees[j].Contains(tmpValue[0])) first = j;
                        if (listOfTrees[j].Contains(tmpValue[1])) second = j;
                    }
                    if (contain)
                    {
                        if (first > second)
                        {
                            int tmpVal = first;
                            first = second;
                            second = tmpVal;
                        }
                        if (listOfTrees[first].Contains(tmpValue[0]))
                        {
                            nodesInMST[tmpValue[1]] = true;
                        }
                        else nodesInMST[tmpValue[0]] = true;
                        for (int j = 0; j < listOfTrees[second].Count; j++)
                        {
                            if (!listOfTrees[first].Contains(listOfTrees[second][j])) listOfTrees[0].Add(listOfTrees[second][j]);
                        }
                        listOfTrees.RemoveAt(second);
                        result.Add(tmpValue);
                    }
                }
                //jesli zawiera jeden z wierzcholkow to dodaj
                //jesli zawiera 2 wierzcholki to sprawdz czy te wierzcholki nie maja wspolnych
                foreach (var item in nodesInMST)
                {
                    if (item == false)
                    {
                        check = false;
                        break;
                    }
                }
                if (check) break;
            }

            string returnResult = "Lista krawędzi drzewa drzewa rozpinającego:\n";
            int sum = 0;
            for (int i = 0; i < result.Count; i++)
            {
                returnResult += result[i][0] + " , " + result[i][1] + "\n";
                sum += result[i][2];
            }
            returnResult += "Suma wag krawędzi: " + sum;

            return returnResult;
        }

        // wyznaczanie minimalnego drzewa rozpinającego (MST) – algorytm Prima
        public string CountPrim()
        {
            const int MAX = 2000000000;
            List<int> key = new List<int>();
            List<int> p = new List<int>();
            bool[] visited = new bool[Node];
            int[,] result = new int[2, Node];
            int minimum, tmpValue;

            for (int i = 0; i < Node; i++)
            {
                key.Add(MAX);
                p.Add(-1);
                visited[i] = false;
            }
            key[0] = 0;
            int tmpNode = 0;

            for (int i = 0; i < Node - 1; i++)
            {
                minimum = 2000000000;
                tmpValue = 2000000000;
                visited[tmpNode] = true;
                for (int j = 0; j < list[tmpNode].Count; j += 2)
                {
                    if (!visited[list[tmpNode][j]])
                    {
                        if (list[tmpNode][j+1] < key[list[tmpNode][j]])
                        {
                            key[list[tmpNode][j]] = list[tmpNode][j + 1];
                            p[list[tmpNode][j]] = tmpNode;
                        }
                    }
                }
                for (int j = 0; j < Node; j++)
                {
                    if (key[j] < tmpValue && !visited[j])
                    {
                        tmpValue = key[j];
                        minimum = j;
                    }
                }
                tmpNode = minimum;
            }

            for (int i = 0; i < Node; i++)
            {
                result[0, i] = key[i];
                result[1, i] = p[i];
            }

            return DisplayMST(result);
        }

        //wyznaczanie najkrótszej ścieżki w grafie - algorytm Dijkstry
        public string CountDijkstra()
        {
            for (int i = 0; i < Node; i++)
            {
                for(int j=1; j< list[i].Count; j+=2)
                {
                    if (list[i][j] < 0) return "W grafie wystepują krawędzie z ujemnymi wagami\nAlgorytm Dijkstry nie zadziała";
                }
            }
            int[,] result = new int[2, Node];
            const int MAX = 2000000000;
            List<int> nodes = new List<int>();
            List<int> d = new List<int>();
            List<int> p = new List<int>();

            for (int i = 0; i < Node; i++)
            {
                nodes.Add(i);
                d.Add(MAX);
                p.Add(-1);
            }

            d[Start] = 0;
            int tmpNode = Start;
            
            for (int i = 0; i < Node; i++)
            {
                for (int j = 0; j < Node; j++)
                {
                    if (d[j] < d[tmpNode] && nodes.Contains(j)) tmpNode = j;
                }

                for (int j = 0; j < list[tmpNode].Count; j+=2)
                {
                    int tmpValue = d[tmpNode] + list[tmpNode][j + 1];
                    if (tmpValue < d[list[tmpNode][j]])
                    {
                        d[list[tmpNode][j]] = tmpValue;
                        p[list[tmpNode][j]] = tmpNode;
                    }
                }
                nodes.Remove(tmpNode);
                if(nodes.Count != 0) tmpNode = nodes[0];
            }

            for (int i = 0; i<Node; i++)
            {
                result[0, i] = d[i];
                result[1, i] = p[i];
            }
            return DisplayShortestTrack(result, Start);
        }

        //wyznaczanie najkrótszej ścieżki w grafie - algorytm Bellmanna-Forda
        public string CountBellmann_Ford()
        {
            int[,] result = new int[2, Node];
            const int MAX = 2000000000;
            List<int> d = new List<int>();
            List<int> p = new List<int>();
            bool changed = false;

            for (int i = 0; i < Node; i++)
            {
                d.Add(MAX);
                p.Add(-1);
            }
            d[Start] = 0;

            for (int k = 0; k < Node - 1; k++)
            {
                for (int i = 0; i < Node; i++)
                {
                    for (int j = 0; j < list[i].Count; j += 2)
                    {
                        int tmpValue = d[i] + list[i][j + 1];
                        if (tmpValue < d[list[i][j]])
                        {
                            d[list[i][j]] = tmpValue;
                            p[list[i][j]] = i;
                            changed = true;
                        }
                    }
                }
                if (changed == false) break;
                changed = false;
            }
            //ujemny cykl
            for (int i = 0; i < Node; i++)
            {
                for (int j = 0; j < Node; j++)
                {
                    int tmpValue = d[i] + list[i][j + 1];
                    if (tmpValue < d[list[i][j]]) return "W grafie występuje cykl ujemny";
                }
            }

            for (int i = 0; i < Node; i++)
            {
                result[0, i] = d[i];
                result[1, i] = p[i];
            }

            return DisplayShortestTrack(result, Start);
        }
    }
}
