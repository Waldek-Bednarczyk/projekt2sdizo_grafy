﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy_Bednarczyk
{
    public class Display
    {
        //metoda odwracajaca kolejnosc znakow w stringu
        public static string ReverseString(string s)
        {
            char[] tmpString = s.ToCharArray();
            Array.Reverse(tmpString);
            return new string(tmpString);
        }

        public static string DisplayShortestTrack(int[,] data, int start)
        {
            string result = "";
            string tmpReversed = "";
            for (int i = 0; i < data.Length / 2; i++)
            {
                tmpReversed = "";
                result += "Droga do wierzchołka " + i + ": ";
                int x = i;
                if (data[1, x] == -1)
                {
                    result += "brak";
                }
                else
                {
                    tmpReversed += i + "-";
                    for (; ; )
                    {
                        if (data[1, x] == -1) break;
                        else
                        {
                            if (data[1, x] == start)
                            {
                                break;
                            }
                            else
                            {
                                tmpReversed += data[1, x] + "-";
                                x = data[1, x];
                            }
                        }
                    }
                    tmpReversed += start;
                    result += ReverseString(tmpReversed);
                }
                result += ", koszt: " + data[0, i] + "\n";

            }

            return result;
        }

        public static string DisplayMST(int[,] data)
        {
            string result = "Lista krawędzi drzewa drzewa rozpinającego:\n";
            int sum = 0;
            for(int i=1; i< data.Length/2; i++)
            {
                result += i + " , " + data[1, i] + "\n";
                sum += data[0, i];
            }
            result += "Suma wag krawędzi: " + sum;
            return result;
        }
    }
}
