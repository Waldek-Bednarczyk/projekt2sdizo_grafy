﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafy_Bednarczyk
{
    public partial class FrmMainMenu : Form
    {
        public FrmMainMenu()
        {
            InitializeComponent();
        }

        public static int representation;
        public static int algorithRepresentation;

        MatrixRepresentation matrixDirected;
        MatrixRepresentation matrixUnDirected;
        ListRepresentation listDirected;
        ListRepresentation listUnDirected;

        private void buttonRead_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt";
            string path = "";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                path = openFileDialog.FileName;
            }
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    string values = sr.ReadLine();
                    int[] tmp = new int[3];
                    int[] tmpValues = new int[4];
                    string[] numbersSplitted = values.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < 4; i++) tmpValues[i] = Convert.ToInt32(numbersSplitted[i]);
                    //graf skierowany macierzowo
                    matrixDirected = new MatrixRepresentation(tmpValues[1], tmpValues[0], tmpValues[2], tmpValues[3]);
                    //graf nieskierowany macierzowo
                    matrixUnDirected = new MatrixRepresentation(tmpValues[1], tmpValues[0], tmpValues[2], tmpValues[3]);
                    //graf skierowany listowo
                    listDirected = new ListRepresentation(tmpValues[1], tmpValues[0], tmpValues[2], tmpValues[3]);
                    //graf nieskierowany listowo
                    listUnDirected = new ListRepresentation(tmpValues[1], tmpValues[0], tmpValues[2], tmpValues[3]);

                    for (int i=0; i< tmpValues[0]; i++)
                    {
                        values = sr.ReadLine();
                        numbersSplitted = values.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);
                        for (int j = 0; j < 3; j++) tmp[j] = Convert.ToInt32(numbersSplitted[j]);
                        matrixDirected.AddMatrix(tmp[0], tmp[1], tmp[2]);
                        matrixUnDirected.AddMatrix(tmp[0], tmp[1], tmp[2]);
                        matrixUnDirected.AddMatrix(tmp[1], tmp[0], tmp[2]);
                        listDirected.AddList(tmp[0], tmp[1], tmp[2]);
                        listUnDirected.AddList(tmp[0], tmp[1], tmp[2]);
                        listUnDirected.AddList(tmp[1], tmp[0], tmp[2]);
                    }
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Nie udało się wczytać danych", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonDisplayMatrix_Click(object sender, EventArgs e)
        {
            if (matrixUnDirected != null)
            {
                var frmDecision = new FrmDirectedOrUndirected();
                frmDecision.ShowDialog();
                if (representation == 0) labelDisplay.Text = matrixDirected.DisplayMatrix();
                else labelDisplay.Text = matrixUnDirected.DisplayMatrix();
            }
            else MessageBox.Show("Najpierw wczytaj graf", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void buttonDisplayList_Click(object sender, EventArgs e)
        {
            if(listDirected != null)
            {
                var frmDecision = new FrmDirectedOrUndirected();
                frmDecision.ShowDialog();
                if (representation == 0) labelDisplay.Text = listDirected.DisplayList();
                else labelDisplay.Text = listUnDirected.DisplayList();
            }
            else MessageBox.Show("Najpierw wczytaj graf", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void buttonDijkstra_Click(object sender, EventArgs e)
        {
            if (listDirected != null)
            {
                var frmRepresentation = new FrmRepresentation();
                frmRepresentation.ShowDialog();
                if(algorithRepresentation == 0)
                {
                    labelDisplay.Text = matrixDirected.CountDijkstra();
                }
                else labelDisplay.Text = listDirected.CountDijkstra();
            }
            else MessageBox.Show("Najpierw wczytaj graf", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void buttonBellmanFord_Click(object sender, EventArgs e)
        {
            if (listDirected != null)
            {
                var frmRepresentation = new FrmRepresentation();
                frmRepresentation.ShowDialog();
                if (algorithRepresentation == 0)
                {
                    labelDisplay.Text = matrixDirected.CountBellmann_Ford();
                }
                else labelDisplay.Text = listDirected.CountBellmann_Ford();
            }
            else MessageBox.Show("Najpierw wczytaj graf", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void buttonPrim_Click(object sender, EventArgs e)
        {
            if (listDirected != null)
            {
                var frmRepresentation = new FrmRepresentation();
                frmRepresentation.ShowDialog();
                if (algorithRepresentation == 0)
                {
                    labelDisplay.Text = matrixUnDirected.CountPrim();
                }
                else labelDisplay.Text = listUnDirected.CountPrim();
            }
            else MessageBox.Show("Najpierw wczytaj graf", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void buttonKruskal_Click(object sender, EventArgs e)
        {
            if (listDirected != null)
            {
                var frmRepresentation = new FrmRepresentation();
                frmRepresentation.ShowDialog();
                if (algorithRepresentation == 0)
                {
                    labelDisplay.Text = matrixDirected.CountKruskal();
                }
                else labelDisplay.Text = listDirected.CountKruskal();
            }
            else MessageBox.Show("Najpierw wczytaj graf", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
    }
}
