﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafy_Bednarczyk
{
    public partial class FrmRepresentation : Form
    {
        public FrmRepresentation()
        {
            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            FrmMainMenu.algorithRepresentation = comboBoxRepresentation.SelectedIndex;
            this.Close();
        }
    }
}
