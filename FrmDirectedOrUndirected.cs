﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafy_Bednarczyk
{
    public partial class FrmDirectedOrUndirected : Form
    {

        public FrmDirectedOrUndirected()
        {
            InitializeComponent();
        }

        private void buttonConfirm_Click(object sender, EventArgs e)
        {
            FrmMainMenu.representation = comboBoxDecision.SelectedIndex;
            this.Close();
        }
    }
}
